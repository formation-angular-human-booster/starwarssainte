import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VaisseauxComponent } from './components/vaisseaux/vaisseaux.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { HomeComponent } from './components/home/home.component';
import { PlanetDetailComponent } from './components/planet-detail/planet-detail.component';
import { AddPlanetComponent } from './components/add-planet/add-planet.component';
import { AddVaisseauComponent } from './components/add-vaisseau/add-vaisseau.component';
import {FormsModule} from '@angular/forms';
import { InitialesPipe } from './pipes/initiales.pipe';
import { EditPlanetComponent } from './components/edit-planet/edit-planet.component';
import {ToastrModule} from 'ngx-toastr';
import {HttpClientModule} from '@angular/common/http';

/*
  Angular est très complet, on ne va donc pas charger tout le code source pour une une application ça serait trop lourd
  Le fichier app.module va nous permettre de charger seulement les composants, services, pipes, modules que l'on souhaite
  utiliser.
  Tous composants et pipes devront être inscrit dans la section déclaration. Sinon ils ne seront pas utilisable
  Toutes les librairies devront être ajoutés dans le saction import pour être utilisées (Exemple ).
 */
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    VaisseauxComponent,
    PlanetsComponent,
    HomeComponent,
    PlanetDetailComponent,
    AddPlanetComponent,
    AddVaisseauComponent,
    InitialesPipe,
    EditPlanetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
