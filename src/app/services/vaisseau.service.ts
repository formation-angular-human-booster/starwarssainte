import { Injectable } from '@angular/core';
import {Vaisseau} from '../models/vaisseau';

@Injectable({
  providedIn: 'root'
})
export class VaisseauService {
  vehiculs = [
    new Vaisseau(1, 'Faucon millenium', 'Peugeot', true, 10),
    new Vaisseau(2, 'XWing', 'BMW', false, 2)
  ];
  constructor() { }
  getAllVehiculs(): Vaisseau[] {
    return this.vehiculs;
  }
}
