import { Injectable } from '@angular/core';
import {Planet} from '../models/planet';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class PlanetService {
  // atributs planets qui est initialisé avec un tableau d'objet Planet. (models/planet.ts)
  // Le new Planet(...) appellera le constructeur de notre model Planet afin de créer nos objets.
  // Dans tous notre service, nous manipulerons cet attribut pour ajouter, supprimer, éditer, récuppérer une ou toutes nos planètes
  planets = [
    new Planet(1, 'Alderran', 0, 'Auvergnate', 199),
    new Planet(2, 'Tatooine', 100, 'Savoyarde', 1000)
  ];
  apiUrl = 'http://localhost:3000/planet';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  // Fonction qui retournera toutes nos planètes (celle qui sont contenu dans l'attribut planets).
  // On l'utilisera notament dans le conposant qui affichera toutes nos planetes (planets.component.ts).
  getAllPlanets(): Observable<Planet[]> {
   return this.http.get<Planet[]>(this.apiUrl, this.httpOptions).pipe(
     retry(1),
     catchError(this.handleError)
   );
  }

  // Fonction qui prend en paramètre un id et qui retournera un objet Planet.
  getOnePlanetById(id: number): Planet {
    // fonction filter : Crée un nouveau tableau à partir d'une condition à vérifier.
    // La fonction filter retourne un tableau, l'id d'une planète est unique. Conslusion la fonction filter retournera donc un tableau avec un seul élément planete
    // [{planete}]. Or notre fonction ne retourne pas un tableau avec un élément planete mais un objet planète.
    // C'est pourquoi le [0] nous permet de retrouver ce premier objet.
    return this.planets.filter(planet =>  planet.id === id )[0];
  }
  // fonction qui prend en paramètre un objet Planet
  addPlanet(planet: Planet): void {
    // Ajouter cet élément dans notre attribut qui contient le tableau de planète
    this.planets.push(planet);
  }

  deletePlanete(planet: Planet): Planet[] {
    this.planets = this.planets.filter(planetToDelete => planet !== planetToDelete);
    return this.planets ;
  }
  editPlanet(planet: Planet): Planet[] {
    // Reccupére la planète en fonction de son id puis la met à jour
    // (this.planets.filter(planetToUpdate =>  planet.id === planetToUpdate.id )[0]) => this.planets.filter(planet =>  planet.id === id )[0]
    // sauf que derrière on lui change sa valeur.
    this.planets.filter(planetToUpdate =>  planet.id === planetToUpdate.id )[0] = planet;
    return this.planets;
  }

  handleError(error) {
    let errorMessage = '';
    if ( error.error instanceof ErrorEvent ) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
