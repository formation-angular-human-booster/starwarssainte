import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  loadMessage = 'Super contenu';
  constructor() { }

  // Fonction contenu dans mon logger service qui affichera dans la console "Nouveau contenu"
  consoleLoader() {
    console.log(this.loadMessage);
  }
}
