import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {VaisseauxComponent} from './components/vaisseaux/vaisseaux.component';
import {PlanetsComponent} from './components/planets/planets.component';
import {PlanetDetailComponent} from './components/planet-detail/planet-detail.component';
import {AddPlanetComponent} from './components/add-planet/add-planet.component';
import {AddVaisseauComponent} from './components/add-vaisseau/add-vaisseau.component';
import {EditPlanetComponent} from './components/edit-planet/edit-planet.component';

/*
  Contient une constante appelé routes.
  Cette constante est un tableau qui permettra de lier une url à un composant
  l'url sera contenu dans l'attribut path et le composant dans l'attribut component.
  Dans l'exemple ci-dessous, si mon url est : localhost:4200/vaisseaux,
  la balise <router-outlet> contenue dans le fichier app.component.html affichera le composant
  VaisseauxComponent.
  On retrouve aussi une redirection afin de rediriger l'utilisateur vers /home quand il accède
  à localhost:4200.

  Fonctionnement du tableau de routes. Notre navigateur va passer dans chacune de ces lignes
  Si l'attribut path fonctionne, il affichera le composant, sinon il passera à la ligne suivante
  Exemple : Sur l'adresse localhost:4200/planets/1 il ira jusqu'à la ligne
  { path: 'planets/:id', component: PlanetDetailComponent},
  car aucun autre path ne correspond dans les lignes précédentes.
  C'est donc le composant PlanetDetail qui sera affiché
  Les : derrière un "/" permettent de dire que l'on a à faire à une variable. Elle pourra être
  réccupéré dans notre composant grâce au service Angular ActivatedRoute
 */
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home',      component: HomeComponent },
  { path: 'vaisseaux',      component: VaisseauxComponent },
  { path: 'vaisseaux/ajout',      component: AddVaisseauComponent },
  { path: 'planets',      component: PlanetsComponent },
  { path: 'planets/ajout',      component: AddPlanetComponent },
  { path: 'planets/update/:id', component: EditPlanetComponent},
  { path: 'planets/:id', component: PlanetDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
