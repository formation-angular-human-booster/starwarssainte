import { Component, OnInit } from '@angular/core';
import {Planet} from '../../models/planet';
import {ActivatedRoute, Router} from '@angular/router';
import {PlanetService} from '../../services/planet.service';

@Component({
  selector: 'app-edit-planet',
  templateUrl: './edit-planet.component.html',
  styleUrls: ['./edit-planet.component.css']
})
export class EditPlanetComponent implements OnInit {
  // attribut planet que l'on branchera à notre formulaire cf ajout de planete
  // En revanche ici, on initialise pas cette variable avec new Planet() car on ne veut pas en créer une nouvelle
  // Elle sera ajoutée dans la fonction ngOnInit elle sera utilisa dans notre html dans le ngModel
  planet: Planet;
  // 3 injections : ActivatedRoute afin de réccupérer l'id dans notre url
  // PlanetService pour mettre à jour nos objets
  // Router pour rediriger l'utilisateur
  constructor(private activatedRoute: ActivatedRoute, private planetService: PlanetService, private router: Router) { }

  // Ici comme dans le détail, nous réccupérons notre id puis nous assignons à this.
  // Planet la valeur de retour de notre
  // fonction getOneById dans le service planetService (services/planet.ts)
  // en passant en paramètre l'id réccupéré (cf : planet-detail.component.ts)
   ngOnInit(): void {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.planet = this.planetService.getOnePlanetById(id);
  }

  editPlanet() {
    // Je vais mettre à jour le contenu de mes planètes dans mon service
    this.planetService.editPlanet(this.planet);
    // Je redirige l'utilisateur vers /planets qui liste toutes nos planètes
    this.router.navigate(['/planets']);
  }

}
