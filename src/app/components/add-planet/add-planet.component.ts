import { Component, OnInit } from '@angular/core';
import {Planet} from '../../models/planet';
import {Router} from '@angular/router';
import {PlanetService} from '../../services/planet.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-add-planet',
  templateUrl: './add-planet.component.html',
  styleUrls: ['./add-planet.component.css']
})
export class AddPlanetComponent implements OnInit {
  // Attribut planete qui est initialisé à new Planet();
  // Car nous voulons créer une nouvelle planete et partir d'une feuille blanche
  planet = new Planet();

  // Nous injectons le planet service qui nous servira pour ajouter notre planète dans nos données
  // Router (importé depuis '@angular/router'). On l'injecte afin de pouvoir rediriger notre utilisateur
  // une fois que ce dernier aura soumis le formulaire et que les données auront été ajouté.
  // On injecte également le service toastrService depuis 'ngx-toastr' afin d'afficher une notif à l'utilisateur
  constructor(private planetService: PlanetService, private router: Router,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }
  submitPlanet(): void {
    // On appel notre service pour sauvegarder les données du formulaire via l'attribut planet du composant.
    this.planetService.addPlanet(this.planet);
    // On redirige l'utilisateur vers la liste des planètes qui devra avoir un élément en plus
    this.router.navigate(['/planets']);
    // On envoie une notification à l'utilisateur.
    this.toastr.success('Félicitation', 'Planète ajoutée !!');
  }
}
