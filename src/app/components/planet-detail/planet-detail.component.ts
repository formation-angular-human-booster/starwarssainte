import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Planet} from '../../models/planet';
import {PlanetService} from '../../services/planet.service';

@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.css']
})
export class PlanetDetailComponent implements OnInit {
  // Attribut de type planete qui n'est pas initialisé.
  // Cet attribut nous servira dans notre planet-detail.component.html afin d'afficher les détails de notre planète
  planet: Planet;
  // On injecte ici 2 dépendances dans notre constructeur ActivatedRoute qui va permettre de réccupérer des informations de la requête HTTP.
  // Dans ces informations on pourra retrouver l'id passé dans notre url (planets/1 ou l'on pourre réccupérer par exemple 1 corrrespondant à l'id de notre planète).
  // Le service Activated Route sera importé depuis @Angular/Router.
  // Injection de notre planète Service cf: planets.component.ts
  constructor(private route: ActivatedRoute, private planetService: PlanetService) { }

  ngOnInit(): void {
    // On réccupére notre ID dans l'url grâce au service ActivatedRoute
    const id = +this.route.snapshot.paramMap.get('id');
    // On initialise notre attribut planet grâce à notre
    // PlanetService et à la constante id réccupéré au dessus que l'on passera en paramètre.
    this.planet = this.planetService.getOnePlanetById(id);
  }
}
