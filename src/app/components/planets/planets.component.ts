import { Component, OnInit } from '@angular/core';
import {PlanetService} from '../../services/planet.service';
import {Planet} from '../../models/planet';
import {LoggerService} from '../../services/logger.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {
  // Planets : Il s'agit d'un attribut qui contiendra un tableau de planète. Il n'est à ce moment pas encore initialisé.
  // Cette attribut est vide (undefined) mais ne pourra contenir qu'un tableau de planete.
  planets: Planet[];
  // Variable date qui contient l'objet date Javascript (il correspond à la date du jour)
  date = new Date();
  // Nous injectons ici notre planete service qui nous servira pour réccupérer toutes nos planètes.
  // Le logger service qui n'a pas une grande utilité (il fait seulement un console.log).
  // Nous injectons également le toastrService pour afficher des notifications aux visiteurs.
  constructor(private planetService: PlanetService, private loggerService: LoggerService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    // Nous initialisons notre attribut planets (this.planets) en lui assignant le tableau de planets
    // contenu dans planets.service.ts
    console.log("ici je demande les données");

    this.planetService.getAllPlanets().subscribe(data => {
      this.planets = data;
    });
    // On appel notre logger service pour effectuer un console.log
    this.loggerService.consoleLoader();
  }

  // Fonction appelé lors d'une action de click (click) sur un lien contenu dans le fichier planets.component.html.
  // Elle prend en paramètre l'objet planete à supprimer
  deletePlanete(planete: Planet) {
    // Lors d'un click, on appel la fonction deletePlanet de notre PlanetService avec en paramètre la planète à supprimer
    this.planets = this.planetService.deletePlanete(planete);
    // On affiche une notification à l'utilisateur à l'aide du service de Toastr (contenu dans la librairie téléchargée).
    this.toastr.info('Planète supprimé', 'Au revoir');
  }
}
