import { Component, OnInit } from '@angular/core';
import {LoggerService} from '../../services/logger.service';

// cf : app.component.html
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  /*
   * Permet d'injecter les services que nous utiliserons dans nos composants
   * Autrement dit : dans ce composant sera injecté mon service LoggerService je pourrais donc l'utilisser
   */
  constructor(private loggerService: LoggerService) { }

  // La fonctin ngOninit permet d'effectuer une action après le chargement de la page. Equivalent de documentReadyFunction en Jquery
  // Ici j'appel la fonction consoleLoader de mon service LoggerService (app/services/logger.service.ts)
  ngOnInit(): void {
    this.loggerService.consoleLoader();
  }

}
