export class Vaisseau {
  id: number;
  nom: string;
  model: string;
  lightSpeed: boolean;
  nbPassenger: number;
  constructor(id: number, nom: string, model: string, lightSpeed: boolean, nbPassenger: number) {
    this.id = id;
    this.nom = nom;
    this.model = model;
    this.lightSpeed = lightSpeed;
    this.nbPassenger = nbPassenger;
  }
}
