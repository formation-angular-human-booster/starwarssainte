import { Component } from '@angular/core';

/* Décorateur qui permet à Angular de comprendre que l'on à affaire à un composant dans ce fichier
  On retrouve ici 3 éléments
  - selector: Nom de la balise HTLM qui pourra être utilisé par angular pour appelé ce composant.
  C'est pourquoi on retrouve cette balise dans le fichier index.html
  - templateUrl: Sert à définir le fichier qui servira de template (rendu final en HTML).
  Ce template pourra faire appel aux attributs et fonctions de ce composant
  - styleUrls: Tableau de noms de fichiers qui seront destinés à contenir le css de ce composant.
  Ces fichiers de styles ne seront pas pris en compte dans une autre composant.
  -AppComponent: C'est le composant principal de notre application il embarquera tous les composants à afficher.
*/
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  developper = 'Aurélien Delorme';
  title = 'starWars';
}
